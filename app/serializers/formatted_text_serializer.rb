class FormattedTextSerializer < ActiveModel::Serializer
    attributes :id, :user_id, :style
end