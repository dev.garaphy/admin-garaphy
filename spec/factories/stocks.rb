FactoryBot.define do
  factory :stock do
    title {"MyString"}
    description {"MyText"}
    path {"MyString"}
    category_id {1}
    created_at {"2020-05-03 05:24:17"}
    updated_at {"2020-05-03 05:24:17"}
  end
end
