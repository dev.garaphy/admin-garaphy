TITLES = {
    photo: "RESERVED_PHOTOS",
    background: "RESERVED_BACKGROUNDS",
    animation: "RESERVED_ANIMATIONS",
    stock: "RESERVED_STOCKS",
    design: "RESERVED_DESIGNS"
}.freeze

DESIGNERS = {
    subCategory: "Recent Design(s)"
}.freeze
